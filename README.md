# Es Maint Viewer

es-maint-viewer.sh, tmux config script used to enhance visibility during Wikimedia's
Elasticsearch maintenance operations.

## Invoking
Login to any bastion (bastxxxx.wikimedia.org), then run the following:

``` ./es-maint-viewer.sh ${cluster} ```

Where cluster is any of eqiad, codfw, or cloudelastic.

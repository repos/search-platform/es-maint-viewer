#!/usr/bin/env bash
# "es-maint-viewer.sh",
# tmux config script that opens/configures multiple panes. Used to
# enhance visibility during Elasticsearch maintenance operations
# 0.0.2a by Brian King
# Released under the Apache 2.0 license

tmux new-session -s es-maint-${1} -n es-maint-${1}-viewer -d
tmux set-window-option -g aggressive-resize on

# Creates six (6) windows, 2x2x2
tmux split-window -h -t es-maint-${1}
tmux split-pane -v -t es-maint-${1}:0.1
tmux split-pane -v -t es-maint-${1}:0.0
tmux split-pane -h -t es-maint-${1}:0.0
tmux split-pane -h -t es-maint-${1}:0.2
# tmux split-pane -h -t es-maint-${1}:0.4
# tmux split-pane -h -t es-maint-${1}:0.6

tmux select-layout -t es-maint-${1} tiled

function assemble_cmd() {
  shopt -s nocasematch
  # static part of the text
  declare -a full_cmd
  declare -a es_ports=(9243 9443 9643)
  full_cmd[1]="'watch -d -n 5 \" "
  full_cmd[2]="curl -H 'Accept: application/yaml' -XGET -s "
  full_cmd[3]="https://search.svc.${1}.wmnet:"
  full_cmd[4]="${es_ports[n]}/_cat/health?v=true\" ' "
  full_cmd[5]=' C-m'
  case ${1} in
    cloudelastic) es_ports=(8243 8443 8643)
      full_cmd[3]="https://cloudelastic.wikimedia.org:";;
      # printf "%s " "Elasticports are ${es_ports[@]}";;
    eqiad | codfw );;
    * )
      printf "Error: expecting 'cloudelastic', 'codfw', or 'eqiad', got '${1}'. Exiting."
      tmux kill-session -t es-maint-${1}
      exit 1 ;;
  esac
  for n in "${!es_ports[@]}"; do
    full_cmd[0]="tmux send-keys -t es-maint-${1}:0.${n} "
    full_cmd[4]="${es_ports[n]}/_cat/health?v=true\" ' "
  # Bash automatically puts spaces between expanded arrays, we use the below
  # command to remove these unwanted spaces.
    assembled_cmd=$(printf "%s" "${full_cmd[@]}")
    eval ${assembled_cmd}
  done

  # FIXME: DRY this out
  #
  full_cmd[0]="tmux send-keys -t es-maint-${1}:0.3 "
  full_cmd[2]="curl -XGET -s "
  full_cmd[4]="9243/_cat/recovery?active_only&h=source_node,target_node,time,stage,bytes_percent,bytes_total\"' "
  assembled_cmd=$(printf "%s" "${full_cmd[@]}")
  eval ${assembled_cmd}
  
  full_cmd[0]="tmux send-keys -t es-maint-${1}:0.4 "
  full_cmd[4]="9243/_cat/allocation?v=true\"' "
  assembled_cmd=$(printf "%s" "${full_cmd[@]}")
  eval ${assembled_cmd}
  
  full_cmd[0]="tmux send-keys -t es-maint-${1}:0.5 "
  full_cmd[2]="curl -XGET -s "
  full_cmd[4]="9243/_cat/shards?h=index,shard,prirep,state,unassigned.reason | grep -v STARTED\"' "
  assembled_cmd=$(printf "%s" "${full_cmd[@]}")
  eval ${assembled_cmd}

}

assemble_cmd "${1}"

